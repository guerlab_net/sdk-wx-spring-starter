package net.guerlab.smart.wx.cp.spring.service;

import lombok.Data;
import me.chanjar.weixin.cp.api.WxCpService;
import net.guerlab.smart.wx.cp.spring.properties.WxCpAppProperties;

/**
 * 微信API的Service包装
 *
 * @author guer
 */
@Data
public class WxCpServiceWrapper {

    /**
     * 微信API的Service
     */
    private WxCpService service;

    /**
     * 企业微信应用
     */
    private WxCpAppProperties properties;

    /**
     * 获取是否启用企业微信应用
     *
     * @return 是否启用企业微信应用
     */
    public boolean enabled() {
        return properties != null && properties.getEnable() != null && properties.getEnable();
    }
}
