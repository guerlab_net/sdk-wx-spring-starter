package net.guerlab.smart.wx.cp.spring.properties;

import lombok.Data;

/**
 * 企业微信应用
 *
 * @author guer
 */
@Data
public class WxCpAppProperties {

    /**
     * 是否启用企业微信应用
     */
    private Boolean enable = true;

    /**
     * 企业微信的corpId
     */
    private String corpId;

    /**
     * 设置微信企业应用的agentId
     */
    private Integer agentId;

    /**
     * 设置微信企业应用的Secret
     */
    private String secret;

    /**
     * 设置微信企业应用的token
     */
    private String token;

    /**
     * 设置微信企业应用的EncodingAESKey
     */
    private String aesKey;
}
