package net.guerlab.smart.wx.cp.spring.exception;

import net.guerlab.spring.commons.exception.AbstractI18nApplicationException;

/**
 * 非企业用户
 *
 * @author guer
 */
public class IsNotCompanyUserException extends AbstractI18nApplicationException {

    private static final long serialVersionUID = 1L;

    private static final String MESSAGE_KEY = "message.exception.wx.cp.isNotCompanyUserException";

    @Override
    protected String getKey() {
        return MESSAGE_KEY;
    }
}
