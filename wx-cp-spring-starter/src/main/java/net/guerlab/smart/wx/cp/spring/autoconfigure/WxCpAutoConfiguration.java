package net.guerlab.smart.wx.cp.spring.autoconfigure;

import me.chanjar.weixin.cp.api.WxCpService;
import me.chanjar.weixin.cp.api.impl.WxCpServiceImpl;
import net.guerlab.smart.wx.cp.spring.manager.WxCpServiceManager;
import net.guerlab.smart.wx.cp.spring.properties.WxCpProperties;
import net.guerlab.smart.wx.cp.spring.service.WxCpServiceWrapper;
import net.guerlab.smart.wx.cp.spring.storage.WxCpRedisTemplateConfigStorage;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * 企业微信自动配置
 *
 * @author guer
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Configuration
@ConditionalOnProperty(prefix = "wx.cp", value = "enable", havingValue = "true")
@EnableConfigurationProperties(WxCpProperties.class)
public class WxCpAutoConfiguration {

    /**
     * 构造企业微信应用服务管理
     *
     * @param properties
     *         配置项
     * @param redisTemplate
     *         redisTemplate
     * @return 企业微信应用服务管理
     */
    @Bean
    public WxCpServiceManager wxCpServiceManager(WxCpProperties properties,
            RedisTemplate<String, String> redisTemplate) {
        WxCpServiceManager manager = new WxCpServiceManager();
        String corpId = properties.getCorpId();

        properties.getApp().forEach(appProperties -> {
            WxCpRedisTemplateConfigStorage storage = new WxCpRedisTemplateConfigStorage(redisTemplate);
            storage.setCorpId(appProperties.getCorpId() != null ? appProperties.getCorpId() : corpId);
            storage.setAgentId(appProperties.getAgentId());
            storage.setCorpSecret(appProperties.getSecret());
            storage.setToken(appProperties.getToken());
            storage.setAesKey(appProperties.getAesKey());

            WxCpService service = new WxCpServiceImpl();
            service.setWxCpConfigStorage(storage);

            WxCpServiceWrapper wrapper = new WxCpServiceWrapper();
            wrapper.setProperties(appProperties);
            wrapper.setService(service);

            manager.add(wrapper);
        });

        return manager;
    }

}
