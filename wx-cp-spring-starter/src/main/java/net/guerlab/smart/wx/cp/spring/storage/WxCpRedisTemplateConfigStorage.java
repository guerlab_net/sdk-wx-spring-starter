package net.guerlab.smart.wx.cp.spring.storage;

import me.chanjar.weixin.cp.config.impl.WxCpDefaultConfigImpl;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 * 基于RedisTemplate的微信配置provider
 *
 * @author guer
 */
public class WxCpRedisTemplateConfigStorage extends WxCpDefaultConfigImpl {

    private static final String ACCESS_TOKEN_KEY_PREFIX = "wx:cp:access_token:";

    private final RedisTemplate<String, String> redisTemplate;

    private String accessTokenKey;

    public WxCpRedisTemplateConfigStorage(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void setCorpId(String corpId) {
        super.setCorpId(corpId);
        accessTokenKey = ACCESS_TOKEN_KEY_PREFIX.concat(corpId);
    }

    @Override
    public String getAccessToken() {
        return redisTemplate.opsForValue().get(accessTokenKey);
    }

    @Override
    public boolean isAccessTokenExpired() {
        Long expire = redisTemplate.getExpire(accessTokenKey, TimeUnit.SECONDS);
        return expire == null || expire < 2;
    }

    @Override
    public synchronized void updateAccessToken(String accessToken, int expiresInSeconds) {
        redisTemplate.opsForValue().set(accessTokenKey, accessToken, expiresInSeconds - 200, TimeUnit.SECONDS);
    }

    @Override
    public void expireAccessToken() {
        redisTemplate.expire(accessTokenKey, 0, TimeUnit.SECONDS);
    }
}
