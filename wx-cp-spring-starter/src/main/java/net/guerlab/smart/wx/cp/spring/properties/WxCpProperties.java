package net.guerlab.smart.wx.cp.spring.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.util.List;

/**
 * 企业微信配置
 *
 * @author guer
 */
@Data
@RefreshScope
@ConfigurationProperties(prefix = "wx.cp")
public class WxCpProperties {

    /**
     * 是否启用企业微信
     */
    private Boolean enable = true;

    /**
     * 企业微信的corpId
     */
    private String corpId;

    /**
     * 企业微信应用列表
     */
    private List<WxCpAppProperties> app;
}
