package net.guerlab.smart.wx.cp.spring.manager;

import me.chanjar.weixin.cp.api.WxCpService;
import net.guerlab.smart.wx.cp.spring.service.WxCpServiceWrapper;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 企业微信应用服务管理
 *
 * @author guer
 */
public class WxCpServiceManager {

    private final Map<Integer, WxCpServiceWrapper> wrapperMap = new ConcurrentHashMap<>();

    /**
     * 获取企业微信应用服务
     *
     * @param agentId
     *         agentId
     * @return 企业微信应用服务
     */
    public WxCpService getWxCpService(Integer agentId) {
        WxCpServiceWrapper wrapper = wrapperMap.get(agentId);

        if (wrapper == null || !wrapper.enabled()) {
            return null;
        }

        return wrapper.getService();
    }

    /**
     * 添加企业微信应用服务
     *
     * @param wrapper
     *         企业微信应用服务
     */
    public void add(WxCpServiceWrapper wrapper) {
        if (wrapper == null || wrapper.getService() == null || wrapper.getProperties() == null) {
            return;
        }

        wrapperMap.put(wrapper.getProperties().getAgentId(), wrapper);
    }

    /**
     * 移除企业微信应用服务
     *
     * @param agentId
     *         agentId
     */
    public void remove(Integer agentId) {
        wrapperMap.remove(agentId);
    }
}
