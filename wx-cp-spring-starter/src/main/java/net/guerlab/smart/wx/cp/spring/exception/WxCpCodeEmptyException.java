package net.guerlab.smart.wx.cp.spring.exception;

import net.guerlab.spring.commons.exception.AbstractI18nApplicationException;

/**
 * 企业微信Code无效
 *
 * @author guer
 */
public class WxCpCodeEmptyException extends AbstractI18nApplicationException {

    private static final long serialVersionUID = 1L;

    private static final String MESSAGE_KEY = "message.exception.wx.cp.wxCpCodeEmpty";

    @Override
    protected String getKey() {
        return MESSAGE_KEY;
    }
}
