package net.guerlab.smart.wx.mp.spring.storage;

import me.chanjar.weixin.mp.config.impl.WxMpDefaultConfigImpl;
import me.chanjar.weixin.mp.enums.TicketType;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 * 基于RedisTemplate的微信配置provider
 *
 * @author guer
 */
public class WxMpRedisTemplateConfigStorage extends WxMpDefaultConfigImpl {

    private static final String ACCESS_TOKEN_KEY_PREFIX = "wx:mp:access_token:";

    private final RedisTemplate<String, String> redisTemplate;

    private String accessTokenKey;

    public WxMpRedisTemplateConfigStorage(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void setAppId(String appId) {
        super.setAppId(appId);
        accessTokenKey = ACCESS_TOKEN_KEY_PREFIX.concat(appId);
    }

    private String getTicketRedisKey(TicketType type) {
        return String.format("wx:mp:ticket:key:%s:%s", appId, type.getCode());
    }

    @Override
    public String getAccessToken() {
        return redisTemplate.opsForValue().get(accessTokenKey);
    }

    @Override
    public boolean isAccessTokenExpired() {
        Long expire = redisTemplate.getExpire(accessTokenKey, TimeUnit.SECONDS);
        return expire == null || expire < 2;
    }

    @Override
    public synchronized void updateAccessToken(String accessToken, int expiresInSeconds) {
        redisTemplate.opsForValue().set(accessTokenKey, accessToken, expiresInSeconds - 200, TimeUnit.SECONDS);
    }

    @Override
    public void expireAccessToken() {
        redisTemplate.expire(accessTokenKey, 0, TimeUnit.SECONDS);
    }

    @Override
    public String getTicket(TicketType type) {
        return redisTemplate.opsForValue().get(getTicketRedisKey(type));
    }

    @Override
    public boolean isTicketExpired(TicketType type) {
        Long expire = redisTemplate.getExpire(getTicketRedisKey(type), TimeUnit.SECONDS);
        return expire == null || expire < 2;
    }

    @Override
    public synchronized void updateTicket(TicketType type, String ticket, int expiresInSeconds) {
        redisTemplate.opsForValue().set(getTicketRedisKey(type), ticket, expiresInSeconds - 200, TimeUnit.SECONDS);
    }

    @Override
    public void expireTicket(TicketType type) {
        redisTemplate.expire(getTicketRedisKey(type), 0, TimeUnit.SECONDS);
    }
}
