package net.guerlab.smart.wx.mp.spring.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 微信公众号配置
 *
 * @author guer
 */
@Data
@RefreshScope
@ConfigurationProperties(prefix = "wx.mp")
public class WxMpProperties {

    /**
     * 是否启用微信公众号
     */
    private Boolean enable = true;

    /**
     * 设置微信公众号的appId
     */
    private String appId;

    /**
     * 设置微信公众号的app secret
     */
    private String secret;

    /**
     * 设置微信公众号的token
     */
    private String token;

    /**
     * 设置微信公众号的EncodingAESKey
     */
    private String aesKey;
}
