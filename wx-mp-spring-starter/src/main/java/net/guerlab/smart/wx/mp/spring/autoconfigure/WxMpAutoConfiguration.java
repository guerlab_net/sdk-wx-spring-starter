package net.guerlab.smart.wx.mp.spring.autoconfigure;

import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.config.WxMpConfigStorage;
import net.guerlab.smart.wx.mp.spring.properties.WxMpProperties;
import net.guerlab.smart.wx.mp.spring.storage.WxMpRedisTemplateConfigStorage;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * 微信公众号自动配置
 *
 * @author guer
 */
@Configuration
@ConditionalOnProperty(prefix = "wx.mp", value = "enable", havingValue = "true")
@EnableConfigurationProperties(WxMpProperties.class)
public class WxMpAutoConfiguration {

    /**
     * 构造微信公众号配置
     *
     * @param properties
     *         配置项
     * @param redisTemplate
     *         redisTemplate
     * @return 微信公众号配置
     */
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Bean
    public WxMpConfigStorage wxMpConfigStorage(WxMpProperties properties, RedisTemplate<String, String> redisTemplate) {
        WxMpRedisTemplateConfigStorage storage = new WxMpRedisTemplateConfigStorage(redisTemplate);
        storage.setAppId(properties.getAppId());
        storage.setSecret(properties.getSecret());
        storage.setToken(properties.getToken());
        storage.setAesKey(properties.getAesKey());
        return storage;
    }

    /**
     * 构造微信公众号服务
     *
     * @param wxMpConfigStorage
     *         微信公众号配置
     * @return 微信公众号服务
     */
    @Bean
    public WxMpService wxMpService(WxMpConfigStorage wxMpConfigStorage) {
        WxMpService service = new WxMpServiceImpl();
        service.setWxMpConfigStorage(wxMpConfigStorage);

        return service;
    }

}
