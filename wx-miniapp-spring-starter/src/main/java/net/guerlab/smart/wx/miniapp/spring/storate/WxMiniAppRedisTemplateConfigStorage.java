package net.guerlab.smart.wx.miniapp.spring.storate;

import cn.binarywang.wx.miniapp.config.impl.WxMaDefaultConfigImpl;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 * 基于RedisTemplate的微信配置provider
 *
 * @author guer
 */
public class WxMiniAppRedisTemplateConfigStorage extends WxMaDefaultConfigImpl {

    private static final String ACCESS_TOKEN_KEY_PREFIX = "wx:miniapp:access_token:";

    private static final String CARD_API_KEY_PREFIX = "wx:miniapp:cardapi:";

    private static final String JSAPI_KEY_PREFIX = "wx:miniapp:jsapi:";

    private final RedisTemplate<String, String> redisTemplate;

    private String accessTokenKey;

    private String cardApiKey;

    private String jsApiKey;

    public WxMiniAppRedisTemplateConfigStorage(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void setAppid(String appId) {
        super.setAppid(appId);
        accessTokenKey = ACCESS_TOKEN_KEY_PREFIX.concat(appId);
        cardApiKey = CARD_API_KEY_PREFIX.concat(appId);
        jsApiKey = JSAPI_KEY_PREFIX.concat(appId);
    }

    @Override
    public String getAccessToken() {
        return redisTemplate.opsForValue().get(accessTokenKey);
    }

    @Override
    public boolean isAccessTokenExpired() {
        Long expire = redisTemplate.getExpire(accessTokenKey, TimeUnit.SECONDS);
        return expire == null || expire < 2;
    }

    @Override
    public synchronized void updateAccessToken(String accessToken, int expiresInSeconds) {
        redisTemplate.opsForValue().set(accessTokenKey, accessToken, expiresInSeconds - 200, TimeUnit.SECONDS);
    }

    @Override
    public void expireAccessToken() {
        redisTemplate.expire(accessTokenKey, 0, TimeUnit.SECONDS);
    }

    @Override
    public String getCardApiTicket() {
        return redisTemplate.opsForValue().get(cardApiKey);
    }

    @Override
    public boolean isCardApiTicketExpired() {
        Long expire = redisTemplate.getExpire(cardApiKey, TimeUnit.SECONDS);
        return expire == null || expire < 2;
    }

    @Override
    public void updateCardApiTicket(String cardApiTicket, int expiresInSeconds) {
        redisTemplate.opsForValue().set(cardApiKey, cardApiTicket, expiresInSeconds - 200, TimeUnit.SECONDS);
    }

    @Override
    public void expireCardApiTicket() {
        redisTemplate.expire(cardApiKey, 0, TimeUnit.SECONDS);
    }

    @Override
    public String getJsapiTicket() {
        return redisTemplate.opsForValue().get(jsApiKey);
    }

    @Override
    public boolean isJsapiTicketExpired() {
        Long expire = redisTemplate.getExpire(jsApiKey, TimeUnit.SECONDS);
        return expire == null || expire < 2;
    }

    @Override
    public void updateJsapiTicket(String jsapiTicket, int expiresInSeconds) {
        redisTemplate.opsForValue().set(jsApiKey, jsapiTicket, expiresInSeconds - 200, TimeUnit.SECONDS);
    }

    @Override
    public void expireJsapiTicket() {
        redisTemplate.expire(jsApiKey, 0, TimeUnit.SECONDS);
    }
}
