package net.guerlab.smart.wx.miniapp.spring.exception;

import net.guerlab.spring.commons.exception.AbstractI18nApplicationException;

/**
 * 微信小程序OpenId无效
 *
 * @author guer
 */
public class WxMiniAppOpenIdInvalidException extends AbstractI18nApplicationException {

    private static final long serialVersionUID = 1L;

    private static final String MESSAGE_KEY = "message.exception.wx.miniapp.wxMiniAppOpenIdInvalid";

    @Override
    protected String getKey() {
        return MESSAGE_KEY;
    }
}
