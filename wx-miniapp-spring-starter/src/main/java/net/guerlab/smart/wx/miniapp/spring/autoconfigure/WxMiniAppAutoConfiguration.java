package net.guerlab.smart.wx.miniapp.spring.autoconfigure;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.config.WxMaConfig;
import net.guerlab.smart.wx.miniapp.spring.properties.WxMiniAppProperties;
import net.guerlab.smart.wx.miniapp.spring.storage.ISessionKeyStorage;
import net.guerlab.smart.wx.miniapp.spring.storage.SessionKeyRedisTemplateStorage;
import net.guerlab.smart.wx.miniapp.spring.storate.WxMiniAppRedisTemplateConfigStorage;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * 微信小程序自动配置
 *
 * @author guer
 */
@Configuration
@ConditionalOnProperty(prefix = "wx.miniapp", value = "enable", havingValue = "true")
@EnableConfigurationProperties(WxMiniAppProperties.class)
public class WxMiniAppAutoConfiguration {

    /**
     * 构造微信小程序配置
     *
     * @param properties
     *         配置项
     * @param redisTemplate
     *         redisTemplate
     * @return 微信小程序配置
     */
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Bean
    @ConditionalOnMissingBean
    public WxMaConfig wxMaConfig(WxMiniAppProperties properties, RedisTemplate<String, String> redisTemplate) {
        WxMiniAppRedisTemplateConfigStorage storage = new WxMiniAppRedisTemplateConfigStorage(redisTemplate);
        storage.setAppid(properties.getAppId());
        storage.setSecret(properties.getSecret());
        storage.setToken(properties.getToken());
        storage.setAesKey(properties.getAesKey());
        storage.setMsgDataFormat(properties.getMsgDataFormat());
        return storage;
    }

    /**
     * 构造微信小程序服务
     *
     * @param wxMaConfig
     *         微信小程序配置
     * @return 微信小程序服务
     */
    @Bean
    @ConditionalOnMissingBean
    public WxMaService wxMaService(WxMaConfig wxMaConfig) {
        WxMaService service = new WxMaServiceImpl();
        service.setWxMaConfig(wxMaConfig);

        return service;
    }

    /**
     * 构造SessionKey储存接口
     *
     * @param redisTemplate
     *         redisTemplate
     * @return SessionKey储存接口
     */
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Bean
    @ConditionalOnMissingBean
    public ISessionKeyStorage sessionKeyStorage(RedisTemplate<String, String> redisTemplate) {
        return new SessionKeyRedisTemplateStorage(redisTemplate);
    }

}
