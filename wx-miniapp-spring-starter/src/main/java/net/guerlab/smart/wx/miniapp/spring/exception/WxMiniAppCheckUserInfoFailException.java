package net.guerlab.smart.wx.miniapp.spring.exception;

import net.guerlab.spring.commons.exception.AbstractI18nApplicationException;

/**
 * 解密微信小程序用户信息失败
 *
 * @author guer
 */
public class WxMiniAppCheckUserInfoFailException extends AbstractI18nApplicationException {

    private static final long serialVersionUID = 1L;

    private static final String MESSAGE_KEY = "message.exception.wx.miniapp.wxMiniAppCheckUserInfoFail";

    @Override
    protected String getKey() {
        return MESSAGE_KEY;
    }

    @Override
    public int getErrorCode() {
        return 499;
    }
}
