package net.guerlab.smart.wx.miniapp.spring.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 微信小程序配置
 *
 * @author guer
 */
@Data
@RefreshScope
@ConfigurationProperties(prefix = "wx.miniapp")
public class WxMiniAppProperties {

    /**
     * 是否启用微信小程序
     */
    private Boolean enable = true;

    /**
     * 微信小程序的appId
     */
    private String appId;

    /**
     * 微信小程序的Secret
     */
    private String secret;

    /**
     * 微信小程序消息服务器配置的token
     */
    private String token;

    /**
     * 微信小程序消息服务器配置的EncodingAESKey
     */
    private String aesKey;

    /**
     * 消息格式，XML或者JSON
     */
    private String msgDataFormat;
}
