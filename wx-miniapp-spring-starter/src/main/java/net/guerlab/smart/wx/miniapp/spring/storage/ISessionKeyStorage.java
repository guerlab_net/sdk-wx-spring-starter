package net.guerlab.smart.wx.miniapp.spring.storage;

/**
 * SessionKey储存接口
 *
 * @author guer
 */
public interface ISessionKeyStorage {

    /**
     * 根据appId和openId设置sessionKey
     *
     * @param appId
     *         appId
     * @param openId
     *         openId
     * @param sessionKey
     *         sessionKey
     */
    void put(String appId, String openId, String sessionKey);

    /**
     * 根据appId和openId查询sessionKey
     *
     * @param appId
     *         appId
     * @param openId
     *         openId
     * @return sessionKey
     */
    String get(String appId, String openId);

    /**
     * 根据appId和openId删除sessionKey
     *
     * @param appId
     *         appId
     * @param openId
     *         openId
     */
    void delete(String appId, String openId);
}
