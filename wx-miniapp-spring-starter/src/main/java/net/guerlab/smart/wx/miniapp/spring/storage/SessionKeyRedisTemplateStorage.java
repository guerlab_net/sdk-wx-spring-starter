package net.guerlab.smart.wx.miniapp.spring.storage;

import org.springframework.data.redis.core.RedisTemplate;

/**
 * SessionKey redis储存接口
 *
 * @author guer
 */
public class SessionKeyRedisTemplateStorage implements ISessionKeyStorage {

    private static final String KEY_FORMATTER = "wx:miniapp:sessionKey:%s:%s";

    private final RedisTemplate<String, String> redisTemplate;

    public SessionKeyRedisTemplateStorage(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    private static String getKey(String appId, String openId) {
        return String.format(KEY_FORMATTER, appId, openId);
    }

    @Override
    public void put(String appId, String openId, String sessionKey) {
        redisTemplate.opsForValue().set(getKey(appId, openId), sessionKey);
    }

    @Override
    public String get(String appId, String openId) {
        return redisTemplate.opsForValue().get(getKey(appId, openId));
    }

    @Override
    public void delete(String appId, String openId) {
        redisTemplate.delete(getKey(appId, openId));
    }
}
