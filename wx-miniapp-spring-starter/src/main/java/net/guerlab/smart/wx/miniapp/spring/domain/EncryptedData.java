package net.guerlab.smart.wx.miniapp.spring.domain;

import lombok.Data;

/**
 * 加密信息请求
 *
 * @author guer
 */
@Data
public class EncryptedData {

    /**
     * 不包括敏感信息的原始数据字符串，用于计算签名
     */
    private String rawData;

    /**
     * 使用 sha1( rawData + session-key ) 得到字符串，用于校验用户信息
     */
    private String signature;

    /**
     * 包括敏感数据在内的完整用户信息的加密数据
     */
    private String encryptedData;

    /**
     * 加密算法的初始向量
     */
    private String iv;

}
