# sdk-wx-spring-start ![](https://img.shields.io/maven-central/v/net.guerlab.sdk/wx-spring-starter.svg)![](https://img.shields.io/badge/LICENSE-LGPL--3.0-brightgreen.svg)
> spring boot下微信开放平台、企业微信、微信公众号、微信小程序的开箱既用环境

## 使用场景
> spring boot应用中需要接入微信
