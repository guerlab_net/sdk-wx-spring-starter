<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>net.guerlab.sdk</groupId>
    <artifactId>wx-spring-starter</artifactId>
    <packaging>pom</packaging>
    <version>${revision}</version>

    <name>${project.groupId}:${project.artifactId}</name>
    <description>weChat component with spring boot</description>
    <url>https://gitee.com/guerlab_net/sdk-dingtalk</url>

    <parent>
        <groupId>net.guerlab</groupId>
        <artifactId>sdk-dependencies</artifactId>
        <version>2.0.0</version>
    </parent>

    <licenses>
        <license>
            <name>GNU LESSER GENERAL PUBLIC LICENSE Version 3</name>
            <url>https://www.gnu.org/licenses/lgpl-3.0.txt</url>
        </license>
    </licenses>

    <developers>
        <developer>
            <id>guer</id>
            <name>guer</name>
            <email>master@guerlab.net</email>
            <organization>guerlab</organization>
            <organizationUrl>http://www.guerlab.net</organizationUrl>
        </developer>
    </developers>

    <organization>
        <name>guerlab</name>
        <url>http://www.guerlab.net</url>
    </organization>

    <scm>
        <connection>scm:git:https://gitee.com/guerlab_net/sdk-dingtalk.git</connection>
        <developerConnection>scm:git:https://gitee.com/guerlab_net/sdk-dingtalk.git</developerConnection>
        <url>https://gitee.com/guerlab_net/sdk-dingtalk</url>
        <tag>HEAD</tag>
    </scm>

    <issueManagement>
        <system>gitee</system>
        <url>https://gitee.com/guerlab_net/wx-spring-starter/issues</url>
    </issueManagement>

    <properties>
        <revision>0.2.0</revision>
        <main.basedir>${basedir}</main.basedir>

        <java.version>1.8</java.version>
        <project.encoding>UTF-8</project.encoding>

        <maven-compiler-plugin.version>3.8.0</maven-compiler-plugin.version>
        <maven-surefire-plugin.version>2.22.1</maven-surefire-plugin.version>
        <maven-javadoc-plugin.version>3.0.1</maven-javadoc-plugin.version>
        <maven-deploy-plugin.version>3.0.0-M1</maven-deploy-plugin.version>
        <maven-gpg-plugin.version>1.6</maven-gpg-plugin.version>

        <guerlab-spring.version>3.1.0</guerlab-spring.version>
        <weixin-java.version>3.7.0</weixin-java.version>
    </properties>

    <distributionManagement>
        <snapshotRepository>
            <id>sonatype-nexus-snapshots</id>
            <name>Sonatype Nexus Snapshots</name>
            <url>https://oss.sonatype.org/content/repositories/snapshots/</url>
        </snapshotRepository>
        <repository>
            <id>sonatype-nexus-staging</id>
            <name>Nexus Release Repository</name>
            <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
        </repository>
    </distributionManagement>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-gpg-plugin</artifactId>
                    <version>${maven-gpg-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-failsafe-plugin</artifactId>
                    <executions>
                        <execution>
                            <goals>
                                <goal>integration-test</goal>
                                <goal>verify</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <configuration>
                        <source>${java.version}</source>
                        <target>${java.version}</target>
                        <encoding>${project.encoding}</encoding>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <configuration>
                        <encoding>${project.encoding}</encoding>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-javadoc-plugin</artifactId>
                    <version>${plugin.javadoc.version}</version>
                    <configuration>
                        <encoding>${project.encoding}</encoding>
                        <aggregate>true</aggregate>
                        <charset>${project.encoding}</charset>
                        <docencoding>${project.encoding}</docencoding>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-gpg-plugin</artifactId>
                <executions>
                    <execution>
                        <id>sign-artifacts</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>sign</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>flatten-maven-plugin</artifactId>
                <version>1.1.0</version>
                <configuration>
                    <updatePomFile>true</updatePomFile>
                    <flattenMode>resolveCiFriendliesOnly</flattenMode>
                </configuration>
                <executions>
                    <execution>
                        <id>flatten</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>flatten</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>flatten.clean</id>
                        <phase>clean</phase>
                        <goals>
                            <goal>clean</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <modules>
        <module>wx-cp-spring-starter</module>
        <module>wx-miniapp-spring-starter</module>
        <module>wx-mp-spring-starter</module>
    </modules>

    <dependencies>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>net.guerlab.spring</groupId>
                <artifactId>guerlab-spring</artifactId>
                <version>${guerlab-spring.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>com.github.binarywang</groupId>
                <artifactId>weixin-java-miniapp</artifactId>
                <version>${weixin-java.version}</version>
            </dependency>
            <dependency>
                <groupId>com.github.binarywang</groupId>
                <artifactId>weixin-java-open</artifactId>
                <version>${weixin-java.version}</version>
            </dependency>
            <dependency>
                <groupId>com.github.binarywang</groupId>
                <artifactId>weixin-java-mp</artifactId>
                <version>${weixin-java.version}</version>
            </dependency>
            <dependency>
                <groupId>com.github.binarywang</groupId>
                <artifactId>weixin-java-cp</artifactId>
                <version>${weixin-java.version}</version>
            </dependency>

            <dependency>
                <groupId>net.guerlab.sdk</groupId>
                <artifactId>wx-cp-spring-starter</artifactId>
                <version>${revision}</version>
            </dependency>
            <dependency>
                <groupId>net.guerlab.sdk</groupId>
                <artifactId>wx-miniapp-spring-starter</artifactId>
                <version>${revision}</version>
            </dependency>
            <dependency>
                <groupId>net.guerlab.sdk</groupId>
                <artifactId>wx-mp-spring-starter</artifactId>
                <version>${revision}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>
</project>
